import { Empresa } from './empresa';
import { Endereco } from './endereco';
import { EntidadeBase } from './entidadeBase';

export class Usuario extends EntidadeBase {
  name: string;
  username: string;
  email: string;
  phone: string;
  website: string;
  address: Endereco;
  company: Empresa;
}
