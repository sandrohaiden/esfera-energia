
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { QueryOptions } from './query-options';

export class Resource {
  id?: any;
  parentId?: number;
}

export class ResourceService<T extends Resource> {

  api: string = 'https://jsonplaceholder.typicode.com/users';

  constructor(
    public httpClient: HttpClient,
    public endpoint: string) { }

  public create(item: T): Observable<T> {
    return this.httpClient
      .post<T>(`${this.api}/${this.endpoint}`, item);
  }

  public update(item: T): Observable<T> {
    return this.httpClient
      .put<T>(`${this.api}/${this.endpoint}/${item.id}`,
        item);
  }

  read(id: number): Observable<T> {
    return this.httpClient
      .get<T>(`${this.api}/${this.endpoint}/${id}`);
  }

  list(queryOptions?: QueryOptions): Observable<T[]> {
    return this.httpClient
      .get(`${this.api}/${this.endpoint}`)
      .pipe(
        map((data: any) => data)
      );
  }

  delete(id: number) {
    return this.httpClient
      .delete(`${this.api}/${this.endpoint}/${id}`);
  }
}
