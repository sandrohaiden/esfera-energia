import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceService } from '../../generic-service/generic-service';
import { Usuario } from 'src/app/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class HomeServiceService extends ResourceService<Usuario> {

  constructor(public http: HttpClient) {
    super(http, '');
  }
}
