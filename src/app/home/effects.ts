import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { HomeServiceService } from './service/home-service.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from "@angular/core";
import * as entidadeSimplesAction from './actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class UsuarioEffects {
  constructor(private service: HomeServiceService, private action$: Actions) { }

  ListarEntidadeSimples$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(entidadeSimplesAction.Listar),
      concatMap(action => this.service.list()),
      map(response => entidadeSimplesAction.ListadoSucesso({payload: response, carregando: false}))
    )
  );
}
