import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { UsuarioState } from '../reducers';
import * as actions from '../actions';
import * as selectors from '../selectors';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

  todo$

  constructor(private store: Store<UsuarioState>) { }

  ngOnInit(): void {
    // this.store.dispatch((actions.Listar()));

    // this.todo$ = this.store.pipe(select(selectors.selectAll));
  }

}
