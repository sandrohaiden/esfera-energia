import { Usuario } from '../models/usuario';
import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { Listar, ListadoSucesso } from './actions';
import { createReducer, on } from '@ngrx/store';

export const entityAdapter = createEntityAdapter<Usuario>();

export interface UsuarioState extends EntityState<Usuario> {
  carregando: boolean;
  totalElementos: number;
}

export const initialState: UsuarioState = entityAdapter.getInitialState({
  carregando: false,
  totalElementos: 0,
});

export const usuarioReducer = (baseState, baseAction) => {
  return createReducer(
    initialState,

    on(Listar, (state, action): UsuarioState => {
      return {...state, carregando: true };
    }),

    on(ListadoSucesso, (state, action): UsuarioState => {
      return entityAdapter.addAll(action.payload, {
        ...state,
        carregando: false
      });
    }),


  )(baseState, baseAction);
};

export const { selectIds, selectEntities, selectAll, selectTotal } = entityAdapter.getSelectors();
