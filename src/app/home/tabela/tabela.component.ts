import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { UsuarioState } from '../reducers';
import * as actions from '../actions';
import * as selectors from '../selectors';

@Component({
  selector: 'app-tabela-home',
  templateUrl: './tabela.component.html',
  styleUrls: ['./tabela.component.scss']
})
export class TabelaComponent implements OnInit {

  todo$

  constructor(private store: Store<UsuarioState>) { }

  ngOnInit(): void {
    this.store.dispatch((actions.Listar()));
    this.todo$ = this.store.pipe(select(selectors.selectAll));
  }

}
