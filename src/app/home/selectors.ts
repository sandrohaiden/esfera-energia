import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as reducer from './reducers';

export const selectState = createFeatureSelector<reducer.UsuarioState>(
  'usuarios'
);
export const selectAll = createSelector(selectState, reducer.selectAll);

export const selectById = (id: number) =>
  createSelector(selectState, (state) => {
    return state.entities[id];
  });
