import { Action, createAction, props } from '@ngrx/store';
import { Usuario } from '../models/usuario';

export enum UsuarioActionTypes {
  Add = '[EntidadeSimples] ADD',
  Remove = '[EntidadeSimples] REM',
  Clear = '[EntidadeSimples] CLE',
  Listar = '[EntidadeSimples] Listar',
  ListadoSucesso = '[EntidadeSimples] ListadoSucesso',
}

export const Add = (product: any) => {
  return { type: UsuarioActionTypes.Add, payload: product } as Action;
};

export const Remove = (product: any) => {
  return { type: UsuarioActionTypes.Remove, payload: product } as Action;
};

export const Clear = () => {
  return { type: UsuarioActionTypes.Clear, payload: null } as Action;
};

export const Listar = createAction(UsuarioActionTypes.Listar);

export const ListadoSucesso = createAction(
  UsuarioActionTypes.ListadoSucesso,
  props<{ payload: Usuario[]; carregando: boolean }>()
);
