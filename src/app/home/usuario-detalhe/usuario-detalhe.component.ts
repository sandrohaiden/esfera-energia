import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-usuario-detalhe',
  templateUrl: './usuario-detalhe.component.html',
  styleUrls: ['./usuario-detalhe.component.scss'],
})
export class UsuarioDetalheComponent implements OnInit {
  usuario: Usuario;

  constructor(private activatedRoute: ActivatedRoute, public router: Router) {}

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params['id']) {
      this.usuario = this.activatedRoute.snapshot.data['entidade'];
      console.log('usuario', this.usuario);
      if (!this.usuario) {
        this.router.navigateByUrl('');
      }
    }
  }
}
