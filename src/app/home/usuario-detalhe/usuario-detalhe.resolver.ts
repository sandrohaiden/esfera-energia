import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Usuario } from '../../models/usuario';
import { select, Store } from '@ngrx/store';
import * as reducer from "../reducers"
import { Observable } from 'rxjs';
import {selectById} from "../selectors"
import { first } from "rxjs/operators";

@Injectable()
export class UsuarioDetalheResolver implements Resolve<Usuario> {
  constructor(private store: Store<reducer.UsuarioState>) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Usuario> {
    const id = Number(route.params['id']);
    return this.store.pipe(select(selectById(id)), first());
  }
}
