import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarComponent } from './listar/listar.component';
import { TabelaComponent } from './tabela/tabela.component';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { usuarioReducer } from './reducers';
import { UsuarioEffects } from './effects';
import { UsuarioDetalheComponent } from './usuario-detalhe/usuario-detalhe.component';
import {UsuarioDetalheResolver} from "./usuario-detalhe/usuario-detalhe.resolver"

const routes: Routes = [
  {
    path: '',
    component: ListarComponent,
  },
  {
    path: ':id',
    component: UsuarioDetalheComponent,
    resolve: {
      entidade: UsuarioDetalheResolver,
    },
  },
];

@NgModule({
  declarations: [ListarComponent, TabelaComponent, UsuarioDetalheComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('usuarios', usuarioReducer),
    EffectsModule.forRoot([UsuarioEffects]),
  ],
  providers: [UsuarioDetalheResolver]
})
export class HomeModule {}
